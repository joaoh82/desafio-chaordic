//
//  PhotoViewController.h
//  Chaordic App
//
//  Created by Joao Henrique Machado Silva on 2/21/14.
//  Copyright (c) 2014 Chaordic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *photo;
@property (nonatomic, weak) IBOutlet UITextView *textViewDescription;

@property (nonatomic) NSString *urlPhoto;
@property (nonatomic) NSString *description;

@end

//
//  PhotoViewController.m
//  Chaordic App
//
//  Created by Joao Henrique Machado Silva on 2/21/14.
//  Copyright (c) 2014 Chaordic. All rights reserved.
//

#import "PhotoViewController.h"

@interface PhotoViewController ()

@end

@implementation PhotoViewController

@synthesize photo = _photo;
@synthesize textViewDescription = _textViewDescription;
@synthesize urlPhoto = _urlPhoto;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Photo";
    
    _textViewDescription.text = _description;
    
    NSURL * imageURL = [NSURL URLWithString:_urlPhoto];
    NSData * imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage * image = [UIImage imageWithData:imageData];
    self.photo.image = image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  PhotosViewController.h
//  Chaordic App
//
//  Created by Joao Henrique Machado Silva on 2/13/14.
//  Copyright (c) 2014 Chaordic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoViewController.h"

@interface PhotosViewController : UIViewController

@property(nonatomic, strong) NSMutableArray *arrayPhotos;

@property(nonatomic, strong) UIScrollView *scrollView;

@end

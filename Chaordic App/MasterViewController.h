//
//  MasterViewController.h
//  Chaordic App
//
//  Created by Joao Henrique Machado Silva on 2/13/14.
//  Copyright (c) 2014 Chaordic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotosViewController.h"

@interface MasterViewController : UIViewController <FBLoginViewDelegate>

@property (strong, nonatomic) IBOutlet FBProfilePictureView *profilePictureView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UIButton *photosButton;

@property (strong, nonatomic) NSArray *arrayPhotos;

-(IBAction)openMyPhotos:(id)sender;

@end

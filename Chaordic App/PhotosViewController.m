//
//  PhotosViewController.m
//  Chaordic App
//
//  Created by Joao Henrique Machado Silva on 2/13/14.
//  Copyright (c) 2014 Chaordic. All rights reserved.
//

#import "PhotosViewController.h"

@interface PhotosViewController ()

@end

@implementation PhotosViewController

@synthesize arrayPhotos = _arrayPhotos;
@synthesize scrollView = _scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"My Photos";
    _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    int h = [_arrayPhotos count]/3;
    
    //CALCULATES DE HEIGHT OF THE CONTENT SIZE OF THE SCROLLVIEW BASED ON THE NUMBER OF PHOTOS ON THE ARRAY.
    [_scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, (h*120)+((h/3)*160))];
	_scrollView.clipsToBounds = YES;
	[_scrollView setUserInteractionEnabled:YES];
	[_scrollView setIndicatorStyle:UIScrollViewIndicatorStyleBlack];
	[_scrollView setScrollEnabled:YES];
	[_scrollView setBounces:YES];
	_scrollView.maximumZoomScale = 4.0;
	_scrollView.minimumZoomScale = 0.75;
    int i = 0;
	int x = 35;
	int y = 20;
    int counter = 1;
    for (NSObject *photo in _arrayPhotos) {
        
        //NSLog(@"Photo - %@", [photo valueForKey:@"picture"]);
        NSURL * imageURL = [NSURL URLWithString:[photo valueForKey:@"picture"]];
        NSData * imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage * image = [UIImage imageWithData:imageData];
        UIImageView *photoImage = [[UIImageView alloc] initWithImage:image];
        [photoImage setContentMode:UIViewContentModeScaleAspectFit];
        if (counter == 1) {
            [photoImage setFrame:CGRectMake(x, y, 80, 160)];
        }else if (counter == 4) {
            [photoImage setFrame:CGRectMake(x, y + 40, 240, 160)];
        }else{
            [photoImage setFrame:CGRectMake(x, y + 40, 80, 80)];
        }
        
        photoImage.userInteractionEnabled = YES;
        photoImage.tag = i;
        //ADDING A GESTURE RECOGNIZER TO THE EVERY UIIMAGE
        UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleImageTap:)];
        [imageTap setNumberOfTapsRequired:1];
        [photoImage addGestureRecognizer:imageTap];
        
        [_scrollView addSubview:photoImage];
        
        //CHECKING IF NEED A NEW LINE ON THE GRID
        i++;
        x = x + 80;
		if (x >= 240) {
			y = y + 80;
			x = 35;
            counter++;
		}
        
        //CHEKING IF IT IS THE THIRD LINE TO CHANGE THE SIZE OF THE IMAGE
        if (counter == 3) {
            //y = y + 80;
            counter++;
            x = 35;
        }else if(counter == 4){
            counter = 1;
            y = y + 160;
			x = 35;
        }
        
    }
    
    [self.view addSubview:_scrollView];
}


- (void)handleImageTap:(UIGestureRecognizer *)gestureRecognizer {
    UIImageView *tappedImage = (UIImageView*)gestureRecognizer.view;
    NSObject *photo = [_arrayPhotos objectAtIndex:tappedImage.tag];
    NSLog(@"%@",photo);
    
    NSLog(@"%d", tappedImage.tag);
    
    PhotoViewController *objView = [[PhotoViewController alloc] initWithNibName:@"PhotoViewController" bundle:nil];
    [objView setUrlPhoto:[photo valueForKey:@"source"]];
    [objView setDescription:[photo valueForKey:@"name"]];
    [self.navigationController pushViewController:objView animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

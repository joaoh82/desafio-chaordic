//
//  AppDelegate.h
//  Chaordic App
//
//  Created by Joao Henrique Machado Silva on 2/13/14.
//  Copyright (c) 2014 Chaordic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

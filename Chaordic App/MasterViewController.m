//
//  MasterViewController.m
//  Chaordic App
//
//  Created by Joao Henrique Machado Silva on 2/13/14.
//  Copyright (c) 2014 Chaordic. All rights reserved.
//

#import "MasterViewController.h"

@interface MasterViewController () {

}
@end

@implementation MasterViewController

@synthesize profilePictureView;
@synthesize nameLabel;
@synthesize statusLabel;
@synthesize photosButton;
@synthesize arrayPhotos;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

-(IBAction)openMyPhotos:(id)sender{
    PhotosViewController *objView = [[PhotosViewController alloc] initWithNibName:@"PhotosViewController" bundle:Nil];
    objView.arrayPhotos = (NSMutableArray*)arrayPhotos;
    [self.navigationController pushViewController:objView animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.title = @"Chaordic App";
    
    FBLoginView *loginView =
    [[FBLoginView alloc] initWithReadPermissions:@[@"basic_info", @"email", @"user_likes", @"user_photos", @"friends_photos"]];
    loginView.delegate = self;
    // Align the button in the center horizontally
    loginView.frame = CGRectOffset(loginView.frame, (self.view.center.x - (loginView.frame.size.width / 2)), self.view.center.y+100);
    [self.view addSubview:loginView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Facebook Methods

// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    self.profilePictureView.profileID = user.id;
    self.nameLabel.text = user.name;
}

// Logged-in user experience
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    self.statusLabel.text = @"You're logged in as";
    
    //MAKING A REQUEST ON THE FACEBOOK GRAPH API AND REQUESTING THE USER PHOTOS.
    [FBRequestConnection startWithGraphPath:@"/me/photos"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              /* handle the result */
                              if (error)
                              {
                                  //HANDLING THE ERROR
                                  //showing an alert for failure
                                  NSLog(@"Unable to connect.");
                              }
                              else
                              {
                                  //HANDLING THE SUCCESS OF THE REQUEST
                                  //CREATING A NSDICTIONARY WITH THE RESULT OF THE REQUEST
                                  NSDictionary *dict = [NSDictionary dictionaryWithDictionary:result];
                                  
                                  //CREATING AN NSARRAY WITH A LIST OF IMAGE OBJECTS IN THE RESULT
                                  arrayPhotos = [dict objectForKey:@"data"];
//                                  NSLog(@"%d", pictures.count);
                                  //NSLog(@"user first photo: %@", [pictures objectAtIndex:0]);
                                  
                                  self.photosButton.enabled = YES;
                                  
                                  PhotosViewController *objView = [[PhotosViewController alloc] initWithNibName:@"PhotosViewController" bundle:Nil];
                                  objView.arrayPhotos = (NSMutableArray*)arrayPhotos;
                                  [self.navigationController pushViewController:objView animated:YES];
                              }
                          }];
}

// Logged-out user experience
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    self.profilePictureView.profileID = nil;
    self.nameLabel.text = @"";
    self.statusLabel.text= @"You're not logged in!";
}

// Handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures that happen outside of the app
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

@end
